package com.hendisantika.springbootdockermysql.controller;

import com.hendisantika.springbootdockermysql.domain.Category;
import com.hendisantika.springbootdockermysql.domain.ResponseList;
import com.hendisantika.springbootdockermysql.service.CategoryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-docker-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/20
 * Time: 17.42
 */
@RestController
@RequestMapping("/api/categories/v1")
public class CategoryController {
    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryServiceImpl categoryService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Category> getAllCategories() {
        logger.debug("==== Get all Categories ====");
        logger.info("this is get all categories api /api/categories/v1/all=====");
        return categoryService.list();
    }

    @ResponseBody
    public ResponseList<Category> getPage(@RequestParam(value = "pagesize", defaultValue = "10") int pagesize,
                                          @RequestParam(value = "cursorkey", required = false) String cursorkey) {
        logger.debug("====get page {} , {} ====", pagesize, cursorkey);
        return categoryService.getPage(pagesize, cursorkey);
    }

    //@RequestMapping(value = "/v1/{id}", method = RequestMethod.GET)
    @GetMapping("/{id}")
    public Category getCategory(@PathVariable("id") Long id) {
        logger.debug("====get category detail with id :[{}] ====", id);
        final Category category = categoryService.get(id);
        return category;
    }

    //@RequestMapping(value = "/v1", method = RequestMethod.POST)
    @PostMapping(value = "/v1")
    public ResponseEntity<Category> createCategory(@RequestBody Category category) {
        logger.debug("====create new category object ====");
        categoryService.create(category);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    //@RequestMapping(value = "/v1/{id}", method = RequestMethod.DELETE)
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCategory(@PathVariable Long id) {
        logger.debug("====delete category detail with id :[{}] ====", id);
        categoryService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    //@RequestMapping(value = "/v1/{id}", method = RequestMethod.PUT)
    @PutMapping("/{id}")
    public ResponseEntity updateCategory(@PathVariable Long id, @RequestBody Category category) {
        logger.debug("====update category detail with id :[{}] ====", id);
        category = categoryService.update(id, category);
        if (null == category) {
            return new ResponseEntity("No DCategory found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(category, HttpStatus.OK);
    }

    @PostMapping("/{id}/json")
    public ResponseEntity updateByJson(@PathVariable Long id, @RequestBody Category category) {
        logger.debug("====update category detail with id :[{}] ====", id);
        category = categoryService.update(id, category);
        if (null == category) {
            return new ResponseEntity("No DCategory found for ID " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(category, HttpStatus.OK);
    }

    @PostMapping("/v1/json")
    public ResponseEntity createByJson(@RequestBody Category category) {
        logger.debug("====create new category object with json====");
        categoryService.create(category);
        return new ResponseEntity(category, HttpStatus.OK);
    }

    /**
     * <pre>
     * schema api : Content-Type: application/x-www-form-urlencoded
     * example json value
     *
     *   {
     *       primaryKeyName: "id",
     *       tableName: "Country",
     *       primaryKeyType: "long",
     *       columns: {
     *           comingSoon: "boolean",
     *           flagImageUrl: "text",
     *           isoCode: "text",
     *           name: "text",
     *           state: "long",
     *           tcsUrl: "text",
     *           createdDate: "datetime"
     *        }
     *   }
     *   </pre>
     *
     * @param request
     */
    @GetMapping("v1/schema")
    public ResponseEntity<Map<String, Object>> getSchema(HttpServletRequest request) {
        final Map<String, Object> body = new HashMap<>();
        final Map<String, String> columns = new HashMap<>();

        columns.put("name", "text");
        columns.put("description", "text");
        columns.put("createdDate", "datetime");

        body.put("columns", columns);
        body.put("tableName", "category");
        body.put("primaryKeyName", "id");
        body.put("primaryKeyType", "long");

        return new ResponseEntity<>(body, HttpStatus.OK);
    }

}
