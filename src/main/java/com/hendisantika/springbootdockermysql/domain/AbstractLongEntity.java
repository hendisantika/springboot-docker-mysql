package com.hendisantika.springbootdockermysql.domain;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-docker-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/02/20
 * Time: 14.04
 */
public class AbstractLongEntity extends AbstractCreatedUpdatedEntity {

    private static final long serialVersionUID = -91940750750388101L;
    private Long id;

    public AbstractLongEntity() {
    }

    public AbstractLongEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String subString() {
        return String.format("id:%d", id);
    }

}
