package com.hendisantika.springbootdockermysql.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-docker-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/02/20
 * Time: 14.04
 */
@Entity(name = "category")
@Data
public class Category extends AbstractLongEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;
}