package com.hendisantika.springbootdockermysql.repository;

import com.hendisantika.springbootdockermysql.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-docker-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/02/20
 * Time: 14.08
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}