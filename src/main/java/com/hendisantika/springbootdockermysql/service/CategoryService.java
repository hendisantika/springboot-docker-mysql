package com.hendisantika.springbootdockermysql.service;

import com.hendisantika.springbootdockermysql.domain.Category;
import com.hendisantika.springbootdockermysql.domain.ResponseList;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-docker-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/02/20
 * Time: 14.08
 */
public interface CategoryService {
    Iterable<Category> list();

    Category get(Long id);

    Category create(Category dCategory);

    void delete(Long id);

    Category update(Long id, Category dCategory);

    ResponseList<Category> getPage(int pagesize, String cursorkey);
}
