package com.hendisantika.springbootdockermysql.service;

import com.hendisantika.springbootdockermysql.domain.Category;
import com.hendisantika.springbootdockermysql.domain.ResponseList;
import com.hendisantika.springbootdockermysql.repository.CategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-docker-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/02/20
 * Time: 14.09
 */
@Service("categorySevice")
public class CategoryServiceImpl implements CategoryService {
    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Iterable<Category> list() {
        return categoryRepository.findAll();
    }

    @Override
    public Category get(Long id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public Category create(Category dCategory) {
        return categoryRepository.save(dCategory);
    }

    @Override
    public void delete(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public Category update(Long id, Category dCategory) {
        if (get(id) == null) {
            return null;
        }
        dCategory.setId(id);
        return categoryRepository.save(dCategory);
    }

    public ResponseList<Category> getPage(int pagesize, String offset) {
        if (offset == null) {
            offset = "0";
        }
        logger.debug(" getPage limit : {} offset : {}", pagesize, offset);
        // return dao.getPage(pagesize, cursorkey);
        final Pageable page = createPageRequest(pagesize, Integer.valueOf(offset) / pagesize
        );
        final Page<Category> results = categoryRepository.findAll(page);
        return populatePages(results.getContent(), pagesize, offset, (int) results.getTotalElements());
    }

    private Pageable createPageRequest(int pagesize, int pageIndex) {
        return PageRequest.of(pageIndex, pagesize);
    }

    protected ResponseList<Category> populatePages(final Collection<Category> items, final int pageSize,
                                                   final String cursorKey, final Integer totalCount) {
        if (items == null || items.isEmpty()) {
            return new ResponseList<Category>(items);
        }

        int total = totalCount;

        if ("0".equals(cursorKey) && items.size() < pageSize) {
            total = items.size();
        }

        // limit = data.size();
        logger.debug(" total records count : {} : Integer.parseInt(cursorKey) + items.size() : {} ", total,
                Integer.parseInt(cursorKey) + items.size());
        String nextCursorKey = null;

        if (items.size() == pageSize && Integer.parseInt(cursorKey) + items.size() < total) {
            nextCursorKey = String.format("%s", Integer.parseInt(cursorKey) + items.size());
        }

        logger.debug("next cursorKey {}", nextCursorKey);

        final ResponseList<Category> page = new ResponseList<Category>(items, nextCursorKey);
        page.withTotal(total).withLimit(items.size());

        // populate previous
        if (!"0".equals(cursorKey)) {
            final int previousCursor = Math.abs(Integer.parseInt(cursorKey) - pageSize);
            page.withReverseCursor(String.format("%s", previousCursor));
        }

        return page;
    }

}
