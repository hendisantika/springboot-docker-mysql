CREATE TABLE category (
   id bigint NOT NULL AUTO_INCREMENT,
   name VARCHAR(400) NOT NULL,
   description VARCHAR(400) NULL,
   createdDate timestamp DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (ID)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;